import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomepageComponent} from "./components/homepage/homepage.component";
import {WorkEducationComponent} from "./components/work-education/work-education.component";
import {ContactsComponent} from "./components/contacts/contacts.component";
import {InfoComponent} from "./components/info.component";

const routes: Routes = [
  {
    path: '',
    component: InfoComponent,
    children: [
      { path: '', component: HomepageComponent },
      { path: 'about', component: WorkEducationComponent },
      { path: 'contact', component: ContactsComponent },
      {path: "home", pathMatch: "full", redirectTo: ""},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class InfoRoutingModule { }
