import {Component, OnInit, Renderer2} from '@angular/core';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {DriveService} from "../../services/drive.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(public driveService: DriveService) {

  }

  ngOnInit(): void {
  }

}
