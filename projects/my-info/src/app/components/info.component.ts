import {Component, HostListener} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent {
  title = 'my-info';
  headerBackgroundVisible: boolean = false;

  constructor(private router: Router) {
    router.events.subscribe(value => {
      setTimeout(()=>{
        const element = document.getElementById('main-wrapper');
        document.documentElement.scrollTop = 0;
        this.headerBackgroundVisible = !!element && document.body.clientHeight === element.scrollHeight;
      }, 10)
    });
  }

  @HostListener("window:scroll", []) onWindowScroll() {
    const verticalOffset = window.pageYOffset
      || document.documentElement.scrollTop
      || document.body.scrollTop || 0;
    this.headerBackgroundVisible = verticalOffset !== 0;
  }
}
