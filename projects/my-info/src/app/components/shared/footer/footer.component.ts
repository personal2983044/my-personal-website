import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  dateNow: Date = new Date();

  constructor(
    public router: Router,
    public route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
  }

  redirect(routeUrl: string) {
    this.router.navigate([routeUrl], {
      relativeTo: this.route,
    });
  }
}
