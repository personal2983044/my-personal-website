import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {InfoRoutingModule} from './info-routing.module';
import {InfoComponent} from './components/info.component';
import {HomepageComponent} from './components/homepage/homepage.component';
import {ContactsComponent} from './components/contacts/contacts.component';
import {WorkEducationComponent} from './components/work-education/work-education.component';
import {MatCardModule} from "@angular/material/card";
import {NavbarComponent} from "./components/shared/navbar/navbar.component";
import {FooterComponent} from "./components/shared/footer/footer.component";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    InfoComponent,
    HomepageComponent,
    ContactsComponent,
    WorkEducationComponent,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    InfoRoutingModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [InfoComponent]
})
export class InfoModule { }
